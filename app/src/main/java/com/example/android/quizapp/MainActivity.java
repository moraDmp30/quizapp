package com.example.android.quizapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String QUESTION1_INPUT = "question1_input";
    private static final String QUESTION2_INPUT = "question2_input";
    private static final String QUESTION3_INPUT = "question3_input";
    private static final String QUESTION4_INPUT = "question4_input";
    private static final String QUESTION5_INPUT = "question5_input";
    private static final String QUESTION6_INPUT = "question6_input";
    private static final String QUESTION7_INPUT = "question7_input";
    private static final String QUESTION_NAME_INPUT = "question_name_input";

    private String name = "";
    private String question1Answer = "";
    private String question1Input = "";
    private String question2Answer = "";
    private String question2Input = "";
    private String question3Answer = "";
    private String question3Input = "";
    private String question4Answer = "";
    private String question4Input = "";
    private String question5Answer = "";
    private String question5Input = "";
    private String question6Answer = "";
    private String question6Input = "";
    private String question7Answer = "";
    private String question7Input = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.question1Answer = getText(R.string.question_1_mugiwara).toString();
        this.question2Answer = getText(R.string.question_2_10).toString();
        this.question3Answer = this.implode(",", new String[]{getString(R.string.question_3_nami), getString(R.string.question_3_chopper)});
        this.question4Answer = getText(R.string.question_4_poneglyph).toString();
        this.question5Answer = this.implode(",", new String[]{getString(R.string.question_5_akainu), getString(R.string.question_5_sengoku)});
        this.question6Answer = getText(R.string.question_6_shirohige).toString();
        this.question7Answer = getText(R.string.kuroashi).toString();

        if (savedInstanceState != null) {
            this.name = savedInstanceState.getString(QUESTION_NAME_INPUT, "");
            this.question1Input = savedInstanceState.getString(QUESTION1_INPUT, "");
            this.question2Input = savedInstanceState.getString(QUESTION2_INPUT, "");
            this.question3Input = savedInstanceState.getString(QUESTION3_INPUT, "");
            this.question4Input = savedInstanceState.getString(QUESTION4_INPUT, "");
            this.question5Input = savedInstanceState.getString(QUESTION5_INPUT, "");
            this.question6Input = savedInstanceState.getString(QUESTION6_INPUT, "");
            this.question7Input = savedInstanceState.getString(QUESTION7_INPUT, "");
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Make sure to call the super method so that the states of our views are saved
        super.onSaveInstanceState(outState);
        // Save our own state now
        outState.putSerializable(QUESTION_NAME_INPUT, this.name);
        outState.putSerializable(QUESTION1_INPUT, this.question1Input);
        outState.putSerializable(QUESTION2_INPUT, this.question2Input);
        outState.putSerializable(QUESTION3_INPUT, this.question3Input);
        outState.putSerializable(QUESTION4_INPUT, this.question4Input);
        outState.putSerializable(QUESTION5_INPUT, this.question5Input);
        outState.putSerializable(QUESTION6_INPUT, this.question6Input);
        outState.putSerializable(QUESTION7_INPUT, this.question7Input);
    }

    /**
     * Submits the quiz and prints a message with the results.
     *
     * @param view
     */
    public void submit(View view) {
        int correct = 0;
        int numberOfQuestions = 7;

        this.getNameFromEditText();
        this.getQuestion7FromEditText();
        if (this.question1Input.equals(this.question1Answer)) {
            correct++;
        }
        if (this.question2Input.equals(this.question2Answer)) {
            correct++;
        }
        if (this.question3Input.equals(this.question3Answer)) {
            correct++;
        }
        if (this.question4Input.equals(this.question4Answer)) {
            correct++;
        }
        if (this.question5Input.equals(this.question5Answer)) {
            correct++;
        }
        if (this.question6Input.equals(this.question6Answer)) {
            correct++;
        }
        if (this.question7Input.equals(this.question7Answer)) {
            correct++;
        }


        if (this.name.length() == 0) {
            this.name = getString(R.string.default_name);
        }
        if (correct == numberOfQuestions) {
            Toast.makeText(getApplicationContext(), getString(R.string.all_correct, new Object[]{this.name}), Toast.LENGTH_SHORT).show();
        } else if (correct == 0) {
            Toast.makeText(getApplicationContext(), getString(R.string.zero_correct, new Object[]{this.name}), Toast.LENGTH_SHORT).show();
        } else if (correct == 1) {
            Toast.makeText(getApplicationContext(), getString(R.string.one_correct, new Object[]{this.name}), Toast.LENGTH_SHORT).show();
        } else if (correct <= 3) {
            Toast.makeText(getApplicationContext(), getString(R.string.less_than_3_correct, new Object[]{this.name, correct}), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.more_than_3_correct, new Object[]{this.name, correct}), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Resets the quiz.
     *
     * @param view
     */
    public void reset(View view) {
        this.name = "";
        ((EditText) findViewById(R.id.name)).setText("");
        this.question1Input = "";
        ((RadioButton) findViewById(R.id.radio_question_1_heart)).setChecked(false);
        ((RadioButton) findViewById(R.id.radio_question_1_mugiwara)).setChecked(false);
        ((RadioButton) findViewById(R.id.radio_question_1_shirohige)).setChecked(false);
        this.question2Input = "";
        ((RadioButton) findViewById(R.id.radio_question_2_8)).setChecked(false);
        ((RadioButton) findViewById(R.id.radio_question_2_10)).setChecked(false);
        ((RadioButton) findViewById(R.id.radio_question_2_11)).setChecked(false);
        this.question3Input = "";
        ((CheckBox) findViewById(R.id.question_3_Ace)).setChecked(false);
        ((CheckBox) findViewById(R.id.question_3_Nami)).setChecked(false);
        ((CheckBox) findViewById(R.id.question_3_Chopper)).setChecked(false);
        ((CheckBox) findViewById(R.id.question_3_carrot)).setChecked(false);
        this.question4Input = "";
        ((RadioButton) findViewById(R.id.radio_question_4_baroque)).setChecked(false);
        ((RadioButton) findViewById(R.id.radio_question_4_pirate)).setChecked(false);
        ((RadioButton) findViewById(R.id.radio_question_4_poneglyph)).setChecked(false);
        this.question5Input = "";
        ((CheckBox) findViewById(R.id.question_5_Akainu)).setChecked(false);
        ((CheckBox) findViewById(R.id.question_5_Aokiji)).setChecked(false);
        ((CheckBox) findViewById(R.id.question_5_Kizaru)).setChecked(false);
        ((CheckBox) findViewById(R.id.question_5_Sengoku)).setChecked(false);
        this.question6Input = "";
        ((RadioButton) findViewById(R.id.radio_question_6_shirohige)).setChecked(false);
        ((RadioButton) findViewById(R.id.radio_question_6_shanks)).setChecked(false);
        ((RadioButton) findViewById(R.id.radio_question_6_kaido)).setChecked(false);
        this.question7Input = "";
        ((EditText) findViewById(R.id.question_7_sanji)).setText("");
    }

    /**
     * Gets name from EditText and stores it on variable.
     */
    private void getNameFromEditText() {
        this.name = ((EditText) findViewById(R.id.name)).getText().toString();
    }

    /**
     * Sets question 1 answer.
     *
     * @param view
     */
    public void onQuestion1Clicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        this.question1Input = "";

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radio_question_1_mugiwara:
                if (checked)
                    this.question1Input = getText(R.string.question_1_mugiwara).toString();
                break;
            case R.id.radio_question_1_shirohige:
                if (checked)
                    this.question1Input = getText(R.string.question_1_shirohige).toString();
                break;
            case R.id.radio_question_1_heart:
                if (checked)
                    this.question1Input = getText(R.string.question_1_heart).toString();
                break;
        }
    }

    /**
     * Sets question 2 answer.
     *
     * @param view
     */
    public void onQuestion2Clicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radio_question_2_8:
                if (checked)
                    this.question2Input = getText(R.string.question_2_8).toString();
                break;
            case R.id.radio_question_2_10:
                if (checked)
                    this.question2Input = getText(R.string.question_2_10).toString();
                break;
            case R.id.radio_question_2_11:
                if (checked)
                    this.question2Input = getText(R.string.question_2_11).toString();
                break;
        }
    }

    /**
     * Sets question 3 answer.
     *
     * @param view
     */
    public void onQuestion3Clicked(View view) {
        boolean aceChecked = ((CheckBox) findViewById(R.id.question_3_Ace)).isChecked();
        boolean namiChecked = ((CheckBox) findViewById(R.id.question_3_Nami)).isChecked();
        boolean chopperChecked = ((CheckBox) findViewById(R.id.question_3_Chopper)).isChecked();
        boolean carrotChecked = ((CheckBox) findViewById(R.id.question_3_carrot)).isChecked();
        List<String> stringList = new ArrayList<>();

        if (aceChecked) {
            stringList.add(getText(R.string.question_3_ace).toString());
        }
        if (namiChecked) {
            stringList.add(getText(R.string.question_3_nami).toString());
        }
        if (chopperChecked) {
            stringList.add(getText(R.string.question_3_chopper).toString());
        }
        if (carrotChecked) {
            stringList.add(getText(R.string.question_3_carrot).toString());
        }

        if (stringList.size() == 0) {
            this.question3Input = "";
        } else {
            String[] stringArray = new String[stringList.size()];
            stringList.toArray(stringArray);

            this.question3Input = this.implode(",", stringArray);
        }
    }

    /**
     * Sets question 4 answer.
     *
     * @param view
     */
    public void onQuestion4Clicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radio_question_4_baroque:
                if (checked)
                    this.question4Input = getText(R.string.question_4_baroque).toString();
                break;
            case R.id.radio_question_4_pirate:
                if (checked)
                    this.question4Input = getText(R.string.question_4_pirate).toString();
                break;
            case R.id.radio_question_4_poneglyph:
                if (checked)
                    this.question4Input = getText(R.string.question_4_poneglyph).toString();
                break;
        }
    }

    /**
     * Sets question 5 answer.
     *
     * @param view
     */
    public void onQuestion5Clicked(View view) {
        boolean aokijiChecked = ((CheckBox) findViewById(R.id.question_5_Aokiji)).isChecked();
        boolean akainuChecked = ((CheckBox) findViewById(R.id.question_5_Akainu)).isChecked();
        boolean kizaruChecked = ((CheckBox) findViewById(R.id.question_5_Kizaru)).isChecked();
        boolean sengokuChecked = ((CheckBox) findViewById(R.id.question_5_Sengoku)).isChecked();
        List<String> stringList = new ArrayList<>();

        if (aokijiChecked) {
            stringList.add(getText(R.string.question_5_aokiji).toString());
        }
        if (akainuChecked) {
            stringList.add(getText(R.string.question_5_akainu).toString());
        }
        if (kizaruChecked) {
            stringList.add(getText(R.string.question_5_kizaru).toString());
        }
        if (sengokuChecked) {
            stringList.add(getText(R.string.question_5_sengoku).toString());
        }

        if (stringList.size() == 0) {
            this.question5Input = "";
        } else {
            String[] stringArray = new String[stringList.size()];
            stringList.toArray(stringArray);

            this.question5Input = this.implode(",", stringArray);
        }
    }

    /**
     * Sets question 6 answer.
     *
     * @param view
     */
    public void onQuestion6Clicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radio_question_6_kaido:
                if (checked)
                    this.question6Input = getText(R.string.question_6_kaido).toString();
                break;
            case R.id.radio_question_6_shanks:
                if (checked)
                    this.question6Input = getText(R.string.question_6_shanks).toString();
                break;
            case R.id.radio_question_6_shirohige:
                if (checked)
                    this.question6Input = getText(R.string.question_6_shirohige).toString();
                break;
        }
    }

    /**
     * Gets question 7 from EditText and stores it on variable.
     */
    private void getQuestion7FromEditText() {
        this.question7Input = ((EditText) findViewById(R.id.question_7_sanji)).getText().toString();
        this.question7Input = this.question7Input.trim().toLowerCase();
    }

    /**
     * Converts a set of strings into a single one with the given separator.
     *
     * @param separator
     * @param data
     * @return String
     */
    public String implode(String separator, String... data) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < data.length - 1; i++) {
            //data.length - 1 => to not add separator at the end
            if (!data[i].matches(" *")) {//empty string are ""; " "; "  "; and so on
                sb.append(data[i]);
                sb.append(separator);
            }
        }
        sb.append(data[data.length - 1].trim());

        return sb.toString();
    }
}
